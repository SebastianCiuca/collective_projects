import sys
import os


def stat_func(lst):

    #timp, cmd
    #dict = [cmd : [timp]]
    #dict = [cmd : [freq]]

    d_cmd = {}
    d_freq = {}
    d_return = {}

    #populam dictionarele
    for t in lst:
        
        try:
            temp_list = [t[0]] + d_cmd[t[1]]
            d_cmd[t[1]] = temp_list
        except KeyError:
            d_cmd[t[1]] = [t[0]]

        d_freq[t[1]] = len(d_cmd[t[1]])

    #creem o lista cu primele 60 de comenzi cele mai folosite, ordine descrescatoare dupa frecventa
    sorted_dict = sorted(d_freq.keys(), key=lambda x: d_freq[x],reverse = True)[:60]

    #populam un dictionar de forma (comanda,frecventa)
    for i in xrange(len(sorted_dict)):
        d_return[sorted_dict[i]] = d_freq[sorted_dict[i]]

    return d_return



def testare():
    assert stat_func([('123','cmd1'),('012','cmd2'),('456','cmd1')]) == {'cmd1':2,'cmd2':1}

#stat_func([('123','cmd1'),('012','cmd2'),('456','cmd1')])