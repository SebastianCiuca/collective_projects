import sys
import os


def bulk(lst):

    #timp, cmd
    #dict = [cmd : [timp]]
    #dict = [cmd : [freq]]

    d_cmd = {}
    d_freq = {}
    d_return = {}

    for t in lst:
        d_cmd[t[0]] = [t[1]]
        d_freq[t[0]] = len(d_cmd[t[0]])

    sorted_dict = sorted(d_freq.items(), key=lambda x: d_freq[x],reverse = True)[:60]

    for k,v in d_freq:
        d_return[k] = d_cmd[k]

    return d_return
